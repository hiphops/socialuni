package com.socialuni.social.sdk.model.QO;

import lombok.Data;

@Data
public class ContentAddQO {
    //需要根据中心返回内容更新id的需要继承这个类
    String content;
}